package main

import "github.com/zserge/webview"

func main() {
	wv := webview.New(webview.Settings{
		Debug:     true,
		Width:     800,
		Height:    600,
		Resizable: true,
		Title:     "AM(F)(M)",
		URL:       "http://localhost:3000/",
	})
	wv.Run()
}
