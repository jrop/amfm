import Mousetrap from 'mousetrap'
import produce from 'immer'
import {createStore as createRawReduxStore} from 'redux'

const INITIAL_STATE = {
	cmdPalette: {
		visible: false,
		searchText: '> ',
		selectedIndex: 0,
		availableCommands: [],
	},
	sidebar: {
		visible: true,
	},
}

export const createStore = () => {
	const store = createRawReduxStore((state, action) => {
		switch (action.type) {
			case 'CMD_PALETTE_TOGGLE':
				return produce(state, draft => {
					Object.assign(draft.cmdPalette, {
						visible: !draft.cmdPalette.visible,
						searchText: '> ',
						selectedIndex: 0,
					})
				})
			case 'CMD_PALETTE_SET_SEARCH_TEXT':
				return produce(state, draft => {
					draft.cmdPalette.searchText = action.value
				})
			case 'SIDEBAR_TOGGLE':
				return produce(state, draft => {
					draft.sidebar.visible = !draft.sidebar.visible
				})
			default:
				console.warn('Unknown action:', action)
				return state
		}
	}, INITIAL_STATE)
	store.subscribe(() => console.log(store.getState()))

	Mousetrap.bindGlobal(['mod+shift+p', 'mod+space'], () => {
		store.dispatch({type: 'CMD_PALETTE_TOGGLE'})
		return false
	})
	Mousetrap.bindGlobal('mod+b', () => {
		store.dispatch({type: 'SIDEBAR_TOGGLE'})
		return false
	})
	Mousetrap.bindGlobal('esc', () => {
		const state = store.getState()
		if (state.cmdPalette.visible) store.dispatch({type: 'CMD_PALETTE_TOGGLE'})
		return false
	})
	Mousetrap.bindGlobal('up', () => {
		const state = store.getState()
		if (state.cmdPalette.visible) store.dispatch({type: 'CMD_PALETTE_UP'})
		return false
	})
	Mousetrap.bindGlobal('down', () => {
		const state = store.getState()
		if (state.cmdPalette.visible) store.dispatch({type: 'CMD_PALETTE_DOWN'})
		return false
	})
	return store
}
