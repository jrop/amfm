import React from 'react'
import {connect} from 'react-redux'

const Sidebar = props => (
	<div className="sidebar" style={{display: props.visible ? '' : 'none'}}>
		Sidebar!
	</div>
)
export default connect(s => s.sidebar)(Sidebar)
