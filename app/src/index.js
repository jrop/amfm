import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {createStore} from './store'

import App from './App'
import './index.css'
import 'mousetrap'
import 'mousetrap-global-bind'

ReactDOM.render(
	<Provider store={createStore()}>
		<App />
	</Provider>,
	document.getElementById('root')
)
