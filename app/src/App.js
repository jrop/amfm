import React, {Component} from 'react'
import CmdPalette from './CmdPalette'
import Sidebar from './Sidebar'

class App extends Component {
	render() {
		return [
			<CmdPalette key="cmd-palette" />,
			<div className="app" key="app">
				<Sidebar />
				<main>Main pane</main>
			</div>,
		]
	}
}

export default App
