import React from 'react'
import {connect} from 'react-redux'

class CmdPalette extends React.Component {
	componentDidUpdate() {
		if (this.props.visible) this._tf.focus()
	}
	render() {
		return (
			<div
				style={{display: this.props.visible ? '' : 'none'}}
				className="cmd-palette-container"
				onClick={() => this.props.dispatch({type: 'CMD_PALETTE_TOGGLE'})}>
				<div className="cmd-palette" onClick={e => e.stopPropagation()}>
					<input
						autoFocus={true}
						value={this.props.searchText}
						onChange={e =>
							this.props.dispatch({
								type: 'CMD_PALETTE_SET_SEARCH_TEXT',
								value: e.target.value,
							})
						}
						ref={el => (this._tf = el)}
					/>
					<div>
						{this.props.availableCommands.map((n, i) => (
							<div key={i}>Command {i}</div>
						))}
					</div>
				</div>
			</div>
		)
	}
}

export default connect(s => s.cmdPalette)(CmdPalette)
