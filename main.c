#define WEBVIEW_IMPLEMENTATION
#include "webview.h"

#ifdef WIN32
int WINAPI WinMain(HINSTANCE hInt, HINSTANCE hPrevInst, LPSTR lpCmdLine,
									 int nCmdShow)
{
#else
int main()
{
#endif
	struct webview webview = {
			.title = "AMFM",
			.url = "http://localhost:3000",
			.width = 800,
			.height = 600,
			.debug = true,
			.resizable = true,
	};
	webview_init(&webview);
	while (webview_loop(&webview, 1) == 0)
		;
	webview_exit(&webview);
	return 0;
}